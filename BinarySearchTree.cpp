
#include "Tree.h"
#include <chrono>
using namespace std;


int main(int argc, char* argv[])
{
    Tree<int, std::string> Tree(12, "fe");
    Tree.Insert(9, "d");
    Tree.Insert(239, "c");
    Tree.PrintAllTree();
    std::cout << Tree.Search(9) << std::endl;
    Tree.Remove(239);
    Tree.PrintAllTree();

    return 0;
}
