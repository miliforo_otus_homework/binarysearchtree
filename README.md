| Type     | Adding | Reading | Removing |
|----------|--------|---------|----------|
| Sorted   | 4 ms.  | 0 ms.   | 0 ms.    | 
| Unsorted | 0 ms.  | 0 ms.   | 0 ms.    | 
