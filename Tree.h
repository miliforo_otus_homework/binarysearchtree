﻿#pragma once
#include <iostream>

template <typename T1, typename T2>
class  Tree
{
public:

    Tree(T1 Key, T2 Value) : Key(Key), Value(Value)
    {
        Root = this;
    };

    T1 Key;
    T2 Value;

    Tree<T1, T2>* Root = nullptr;
    Tree<T1, T2>* Parent = nullptr;
    Tree<T1, T2>* LeftTree = nullptr;
    Tree<T1, T2>* RightTree = nullptr;

    void PrintAllTree()
    {

        if (LeftTree != nullptr)
        LeftTree->PrintAllTree();
        
        std::cout << "Key " << Key << " Value " << Value << std::endl;

        if (RightTree != nullptr)
        RightTree->PrintAllTree();
    }

    Tree* CreateAndFillNode(T1 Key, T2 Value)
    {
        Tree* TempNode = new Tree<T1, T2>(Key, Value);
        return TempNode;
    }

    void Insert(Tree* Tree)
    {
        if (Tree == this)
        {
            this->Key = Tree->Key; 
            this->Value = Tree->Value;
            return;
        }
        
        if (Tree->Key > Key)
        {
            if (LeftTree != nullptr)
            {
                LeftTree->Insert(Tree);
            }
            else
            {
                LeftTree = Tree;
            }
            LeftTree->Parent = this;
            LeftTree->Root = Root;
        }
        else
        {
            if (RightTree != nullptr)
            {
                RightTree->Insert(Tree);
            }
            else
            {
                RightTree = Tree;
            }
            RightTree->Parent = this;
            RightTree->Root = Root;
        }
    }
    
    void Insert(T1 Key, T2 Value)
    {
        if (this->Key == Key)
        {
            this->Key = Key; 
            this->Value = Value;
            return;
        }
        
        if (this->Key > Key)
        {
            if (LeftTree != nullptr)
            {
                LeftTree->Insert(Key, Value);
            }
            else
            {
                LeftTree = CreateAndFillNode(Key, Value);
            }
            LeftTree->Parent = this;
            LeftTree->Root = Root;
        }
        else
        {
            if (RightTree != nullptr)
            {
                RightTree->Insert(Key, Value);
            }
            else
            {
                RightTree = CreateAndFillNode(Key, Value);
            }
            RightTree->Parent = this;
            RightTree->Root = Root;
        }
    }

    T2 Search(T1 Key)
    {
        if (Key == this->Key)
        {
            return Value;
        }

        if (this->Key > Key && LeftTree != nullptr)
        {
            return LeftTree->Search(Key);
        }
        if (this->Key < Key && RightTree != nullptr)
        {
            return RightTree->Search(Key);
        }
        return {};
    }
    
    Tree* Remove(T1 Key)
    {
        if (this->Key > Key && LeftTree != nullptr)
        {
            return LeftTree->Remove(Key);
        }
        
        if (this->Key < Key && RightTree != nullptr)
        {
            return RightTree->Remove(Key);
        }
            
        if (Parent != nullptr)
        {
            if (Parent->LeftTree == this)
            {
                Parent->LeftTree = nullptr;
            }
            else
            {
                Parent->RightTree= nullptr;
            }
    
            if (LeftTree != nullptr)
            {
                Parent->Insert(LeftTree->Key, LeftTree->Value);
            }
    
            if (RightTree != nullptr)
            {
                Parent->Insert(RightTree->Key, RightTree->Value);
            }
        }
        else
        {
            if (LeftTree != nullptr)
            {
                Root = LeftTree;
            
                if (RightTree != nullptr)
                {
                    Root->Insert(RightTree->Key, RightTree->Value);
                }
            }
        }
    
        return {};
    }

 

};

